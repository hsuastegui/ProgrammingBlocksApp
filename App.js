import Expo from "expo";
import React from "react";
import { Provider } from "react-redux";
import { StyleSheet, View, StatusBar } from "react-native";
import { NativeRouter, Route } from "react-router-native";
import ProgramCreate from "./src/screens/ProgramCreate";
import ProgramRun from "./src/screens/ProgramRun";
import store from "./src/store";
import { BACKGROUND } from "./src/constants";

class App extends React.Component {
  componentWillMount() {
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.LANDSCAPE);
  }
  render() {
    return (
      <Provider store={store}>
        <NativeRouter>
          <View style={styles.container}>
            <StatusBar barStyle="light-content" />
            <Route exact path="/" component={ProgramCreate} />
            <Route path="/run" component={ProgramRun} />
          </View>
        </NativeRouter>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: BACKGROUND
  }
});

export default App;
