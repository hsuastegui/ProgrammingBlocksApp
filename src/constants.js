/**
 * COLORS
 */
export const PURPLE = "#AB96CA";
export const YELLOW = "#D2B76C";
export const GREEN = "#75AD53";
export const RED = "#D77762";
export const BLUE = "#73B5A9";
export const BLACK = "#241D21";
export const WHITE = "#FFFFFF";
export const GREY = "#CCC";
export const PRIMARY = YELLOW;
export const SECONDARY = GREEN;
export const ACCENT = RED;
export const BACKGROUND = BLACK;

/**
 * DIRECTIVES
 */
export const FORWARD = "FORWARD";
export const BACKWARD = "BACKWARD";
export const UP = "UP";
export const DOWN = "DOWN";
export const BOX_SIZE = 125;

/**
 * ACTIONS
 */
export const ADD_TO_SEQUENCE = "ADD_TO_SEQUENCE";
export const UPDATE_SEQUENCE = "UPDATE_SEQUENCE";
export const CLEAR_SEQUENCE = "CLEAR_SEQUENCE";
export const REARRANGE_SEQUENCE = "REARRANGE_SEQUENCE";
export const SET_DROPZONE = "SET_DROPZONE";
