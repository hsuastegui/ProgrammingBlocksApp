import { createStore } from "redux";
import reducers from "../reducers";

const initialState = {};
const store = createStore(reducers, initialState);

store.subscribe(() => {
  //console.log(store.getState());
});

export default store;
