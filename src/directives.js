import { PURPLE, YELLOW, BLUE, FORWARD, BACKWARD, UP, DOWN } from "./constants";
export default [
  { color: PURPLE, icon: "arrow-up", directive: UP },
  { color: PURPLE, icon: "arrow-right", directive: FORWARD },
  { color: PURPLE, icon: "arrow-down", directive: DOWN },
  { color: PURPLE, icon: "arrow-left", directive: BACKWARD },
  { color: YELLOW, icon: "share", directive: "turn-right" },
  { color: YELLOW, icon: "reply", directive: "turn-left" },
  { color: BLUE, icon: "hand-paper-o", directive: "stop" }
];
