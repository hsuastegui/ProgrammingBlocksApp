import { FORWARD, BACKWARD, UP, DOWN, BOX_SIZE } from "../constants";
const animation = (directive, position) => {
  switch (directive) {
    case FORWARD:
      return {
        x: position.x._value + BOX_SIZE,
        y: position.y._value
      };
    case BACKWARD:
      return {
        x: position.x._value - BOX_SIZE,
        y: position.y._value
      };
    case UP:
      return {
        x: position.x._value,
        y: position.y._value - BOX_SIZE
      };
    case DOWN:
      return {
        x: position.x._value,
        y: position.y._value + BOX_SIZE
      };
  }
};
export default animation;
