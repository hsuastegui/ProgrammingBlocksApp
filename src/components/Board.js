import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesome as Icon } from "@expo/vector-icons";
import Block from "./Block";
import * as actions from "../actions";
import { WHITE, GREY } from "../constants";

const ProgramBoard = ({ blocks, actions }) => {
  const renderBlocks = () => {
    return blocks.map((b, i) => {
      return (
        <Block
          key={i + b.get("color")}
          color={b.get("color")}
          icon={b.get("icon")}
          directive={b.get("directive")}
          onRelease={({ gesture }) => {
            const layout = b.get("layout");
            if (layout.width) {
              const position = Math.floor(gesture.dx * 2 / layout.width) + i;
              actions.rearrangeSequence({ position, i });
            }
          }}
          onLayout={e => {
            actions.updateSequence({ layout: e.nativeEvent.layout, i });
          }}
          style={styles.block}
        />
      );
    });
  };
  return (
    <View
      style={styles.container}
      onLayout={e => actions.setDropZone(e.nativeEvent.layout)}
    >
      <TouchableOpacity
        style={styles.clear}
        onPress={() => {
          actions.clearSequence();
        }}
      >
        <Icon name="trash" size={20} />
        <Text>Clear</Text>
      </TouchableOpacity>
      {renderBlocks()}
    </View>
  );
};

ProgramBoard.defaultProps = {
  blocks: []
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: WHITE,
    alignSelf: "stretch",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 10
  },
  clear: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    position: "absolute",
    bottom: 10,
    right: 10,
    width: 80,
    backgroundColor: GREY,
    padding: 10,
    borderRadius: 5
  },
  block: {
    marginRight: 5
  }
});

const mapStateToProps = state => ({
  blocks: state.sequence
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ProgramBoard);
