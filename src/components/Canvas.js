import React from "react";
import { StyleSheet, View, Animated, Image } from "react-native";
import animation from "../lib/animations";

class Canvas extends React.Component {
  state = {
    position: new Animated.ValueXY({ x: 0, y: 10 }),
    index: 0
  };
  interval = null;
  componentDidMount() {
    if (this.props.sequence.size) {
      //eslint-disable-next-line no-undef
      this.interval = setInterval(() => {
        if (this.state.index < this.props.sequence.size) {
          const block = this.props.sequence.get(this.state.index);
          const directive = block.get("directive");
          const coordinates = animation(directive, this.state.position);
          Animated.timing(this.state.position, {
            toValue: {
              x: coordinates.x,
              y: coordinates.y
            },
            duration: 800
          }).start();
          this.setState({
            index: this.state.index + 1
          });
        } else {
          //eslint-disable-next-line no-undef
          clearInterval(this.interval);
        }
      }, 1000);
    }
  }
  render() {
    return (
      <View>
        <Image
          source={require("../assets/background1.jpg")}
          style={[styles.image, { zIndex: 1 }]}
        />
        <Image
          source={require("../assets/maze1.png")}
          style={[styles.image, { zIndex: 2 }]}
        />
        <Animated.View style={[styles.view, this.state.position.getLayout()]}>
          <Image source={require("../assets/octopus.png")} />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: 1024,
    height: 768,
    position: "absolute",
    top: 0,
    left: 0
  },
  view: {
    position: "absolute",
    zIndex: 3
  }
});

export default Canvas;
