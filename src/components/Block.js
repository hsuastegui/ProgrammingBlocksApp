import React from "react";
import { StyleSheet, View, Animated, PanResponder } from "react-native";
import { FontAwesome as Icon } from "@expo/vector-icons";
import { BLACK } from "../constants";

const Block = props => {
  const position = new Animated.ValueXY();

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: (e, gesture) => {
      position.setValue({ x: gesture.dx, y: gesture.dy });
    },
    onPanResponderRelease: (e, gesture) => {
      props.onRelease({
        color: props.color,
        icon: props.icon,
        directive: props.directive,
        gesture
      });
      Animated.spring(position, { toValue: { x: 0, y: 0 } }).start();
    }
  });

  return (
    <View style={styles.draggableContainer} onLayout={props.onLayout}>
      <Animated.View {...panResponder.panHandlers} style={position.getLayout()}>
        <View
          style={[
            styles.container,
            props.style,
            { backgroundColor: props.color }
          ]}
        >
          <Icon name={props.icon} size={32} color={BLACK} />
        </View>
      </Animated.View>
    </View>
  );
};

Block.defaultProps = {
  style: {}
};

const styles = StyleSheet.create({
  draggableContainer: {},
  container: {
    width: 100,
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 35,
    paddingRight: 35
  }
});

export default Block;
