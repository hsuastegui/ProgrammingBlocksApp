import React from "react";
import { StyleSheet, View } from "react-native";
import { Link } from "react-router-native";
import { FontAwesome as Icon } from "@expo/vector-icons";

const Nav = () => {
  return (
    <View style={styles.container}>
      <Link to="/">
        <Icon name="home" size={32} color="#FFF" />
      </Link>
      <Link to="/run">
        <Icon name="play" size={32} color="#FFF" />
      </Link>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default Nav;
