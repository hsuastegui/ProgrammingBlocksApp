import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View } from "react-native";
import Nav from "./Nav";
import Block from "./Block";
import directives from "../directives";
import { addToSequence } from "../actions";

const BlocksBar = ({ dropZone, addToSequence }) => {
  const onRelease = ({ gesture, color, icon, directive }) => {
    const x = dropZone.get("x");
    if (gesture.moveX > x) {
      addToSequence({
        color: color,
        icon: icon,
        directive: directive,
        layout: {}
      });
    }
  };

  const renderBlocks = () => {
    return directives.map((b, i) => {
      return (
        <Block
          key={i + b.color}
          color={b.color}
          icon={b.icon}
          directive={b.directive}
          onRelease={onRelease}
          onLayout={() => {}}
        />
      );
    });
  };

  return (
    <View style={styles.container}>
      <Nav />
      {renderBlocks()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    paddingLeft: 10,
    paddingRight: 10,
    zIndex: 2
  }
});

const mapStateToProps = state => ({
  dropZone: state.dropZone
});

const mapDispatchToProps = dispatch => ({
  addToSequence: item => dispatch(addToSequence(item))
});

export default connect(mapStateToProps, mapDispatchToProps)(BlocksBar);
