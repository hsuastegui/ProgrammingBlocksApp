import React from "react";
import { StyleSheet, View } from "react-native";
import BlocksBar from "../components/BlocksBar";
import Board from "../components/Board";

const ProgramCreate = () => {
  return (
    <View style={styles.container}>
      <BlocksBar />
      <Board />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start"
  }
});

export default ProgramCreate;
