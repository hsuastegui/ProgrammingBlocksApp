import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View } from "react-native";
import Nav from "../components/Nav";
import Canvas from "../components/Canvas";

const ProgramRun = props => {
  return (
    <View style={styles.container}>
      <Nav />
      <Canvas sequence={props.blocks} />
    </View>
  );
};

ProgramRun.defaultProps = {
  blocks: []
};

const styles = StyleSheet.create({
  container: {}
});

const mapStateToProps = state => ({
  blocks: state.sequence
});

export default connect(mapStateToProps)(ProgramRun);
