import { List, Map } from "immutable";
import {
  ADD_TO_SEQUENCE,
  UPDATE_SEQUENCE,
  CLEAR_SEQUENCE,
  REARRANGE_SEQUENCE
} from "../constants";

const sequence = (state = List(), action) => {
  switch (action.type) {
    case ADD_TO_SEQUENCE: {
      return state.push(Map(action.payload));
    }
    case CLEAR_SEQUENCE: {
      return List();
    }
    case UPDATE_SEQUENCE: {
      const originalBlock = state.get(action.payload.i);
      const newBlock = originalBlock.set("layout", action.payload.layout);
      return state.set(action.payload.i, newBlock);
    }
    case REARRANGE_SEQUENCE: {
      let { position, i } = action.payload;
      if (position === i) {
        return state;
      }
      if (position < 0) {
        position = 0;
      }
      const block = state.get(i);
      const arr = state.delete(i).insert(position, block);
      return arr;
    }
    default: {
      return state;
    }
  }
};

export default sequence;
