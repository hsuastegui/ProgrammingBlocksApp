import { combineReducers } from "redux";
import sequence from "./sequence";
import dropZone from "./dropZone";

export default combineReducers({
  sequence,
  dropZone
});
