import { Map } from "immutable";
import { SET_DROPZONE } from "../constants";

const dropZone = (state = Map(), action) => {
  switch (action.type) {
    case SET_DROPZONE:
      return Map(action.payload);
    default:
      return state;
  }
};

export default dropZone;
