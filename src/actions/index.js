import {
  ADD_TO_SEQUENCE,
  UPDATE_SEQUENCE,
  CLEAR_SEQUENCE,
  REARRANGE_SEQUENCE,
  SET_DROPZONE
} from "../constants";

export const addToSequence = item => ({
  type: ADD_TO_SEQUENCE,
  payload: item
});

export const updateSequence = item => ({
  type: UPDATE_SEQUENCE,
  payload: item
});

export const clearSequence = () => ({
  type: CLEAR_SEQUENCE,
  payload: []
});

export const rearrangeSequence = movement => ({
  type: REARRANGE_SEQUENCE,
  payload: movement
});

export const setDropZone = layout => ({
  type: SET_DROPZONE,
  payload: layout
});
